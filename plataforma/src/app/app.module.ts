import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { CadastroComponent } from './pages/cadastro/cadastro.component';
import { LoginComponent } from './pages/login/login.component';
import { CadastroFazendaComponent } from './pages/cadastro-fazenda/cadastro-fazenda.component';
import { CadastroLaudoComponent } from './pages/cadastro-laudo/cadastro-laudo.component';
import { LaudoComponent } from './pages/laudo/laudo.component';
import { PaginaInicialComponent } from './pages/pagina-inicial/pagina-inicial.component';

import { RouterModule, Routes } from '@angular/router';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'login', component: LoginComponent },
  { path: 'cadastro', component: CadastroComponent},
  { path: 'paginainicial', component: PaginaInicialComponent},
  { path: 'laudo', component: LaudoComponent},
  { path: 'cadastro/fazenda', component: CadastroFazendaComponent},
  { path: 'cadastro/laudo', component: CadastroLaudoComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    CadastroComponent,
    LoginComponent,
    CadastroFazendaComponent,
    CadastroLaudoComponent,
    LaudoComponent,
    PaginaInicialComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(
      appRoutes
    )
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
