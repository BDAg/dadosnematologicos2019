import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CadastroLaudoComponent } from './cadastro-laudo.component';

describe('CadastroLaudoComponent', () => {
  let component: CadastroLaudoComponent;
  let fixture: ComponentFixture<CadastroLaudoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CadastroLaudoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CadastroLaudoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
