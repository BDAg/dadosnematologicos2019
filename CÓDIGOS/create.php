
<?php
    require 'conecta.php';
    
    // Id que identifica se foi chamado do CRUD ou do Index.html, para o retorno
    $id = null;
    $id = $_GET['id'];

    if (!empty($_POST)) {  // Botão Adicionar foi clicado! (Submit)              
		$usuario  = $_POST['usuario'];
		$senha    = $_POST['senha'];
		$perfil   = $_POST['perfil'];
        $email    = $_POST['email'];
        $nome     = $_POST['nome'];
        $cpf_cnpj = $_POST['cpf_cnpj'];

        // *********************************************************
        //***  Validação aqui depois!!! ***
        // *** Verificar se o CPF ou CNPJ é válido
        // *** Verificar se o nome de usuário é válido
        // *** Verificar se a senha é válida (6 cars no mínimo)
        // *** Verificar se CPF ou CNPJ já existe na tabela Clientes
        // *** Verficar se usuario já existe na tabela Usuarios
        // *********************************************************
	
	    $pdo = bdNema::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
        // Insere registro na tabela Usuarios
        $sql = "INSERT INTO Usuarios (usuario, senha, ultimo_acesso, cod_perfil) VALUES (?, ?, ?, ?)";
        
        $ultimo_acesso =  date('d-m-Y H:i:s');  // *** Ajustar timezone - fuso horário errado!!! ***
        
        $q = $pdo->prepare($sql);
        $q->execute(array($usuario,$senha,$ultimo_acesso,$perfil));     
            
        // Recupera o último cod_usuario para passar pra tab. Clientes/Prestadores
        $sql = "SELECT MAX(cod_usuario) AS max_cod_usuario FROM Usuarios";
        $q = $pdo->prepare($sql);
        $q->execute();
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $cod_usuario = $data['max_cod_usuario'];
            
        // Insere registro na tabela Clientes ou Prestadores, conforme o perfil
        if ($perfil == 1) {     // Cliente
            $sql = "INSERT INTO Clientes (nome_cliente, cpf_cliente, email_cliente, cod_usuario) VALUES (?, ?, ?, ?)";
        } else {                // Prestador
            $sql = "INSERT INTO Prestadores (nome_prestador, cnpj_prestador, email_prestador, cod_usuario) VALUES (?, ?, ?, ?)";
        }
        $q = $pdo->prepare($sql);
        $q->execute(array($nome,$cpf_cnpj,$email,$cod_usuario));  
       
        
        bdNema::desconectar();
        
        if ($id == 'crud') {
            header("Location: crud.php");
        } else {
            header("Location: index.html");
        }
    }           // (!empty($_POST))
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
    <title>Adicionar Usuário</title>
</head>
<body>
    <div class="container">
        <div clas="span10 offset1">
          <div class="card">
            <div class="card-header">
                <h3 class="well">Adicionar</h3>
            </div>
            <div class="card-body">
                <?php
                     if ($id == 'crud') {
	                     echo("<form class='form-horizontal' action='create.php?id=crud' method='post'>");
                     } else {
                         echo("<form class='form-horizontal' action='create.php' method='post'>");
                     }
                ?>
                <div class="mb-3">
                <fieldset id="perfil">
                    <label>
                        <input type="radio" value="1" name="perfil" checked>
                     Sou cliente</label> 
                    <label>
                        <input type="radio" value="2" name="perfil">
                        Sou prestador de serviço</label>
                </fieldset>   
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" name="nome" id="nome" placeholder="Nome completo" required>
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" name="cpf_cnpj" id="cpf_cnpj" placeholder="CPF (se é cliente) ou CNPJ (se é prestador de serviços)" required>
            </div>
    		<div class="mb-3">
              <input type="email" class="form-control" name="email" id="email" placeholder="Email" required>
            </div>
            <div class="mb-3">
                <input type="text" class="form-control" name="usuario" id="usuario" placeholder="Login (sem acentos ou espaços. Ex.: fulano77 ou joao_henrique)" required>
            </div>
            <div class="mb-3">
                <input type="password" class="form-control" name="senha" id="senha" placeholder="Senha (mínimo de 6 caracteres)" required>
            </div>
                <div class="form-actions">
                    <br/>
                    <button type="submit" class="btn btn-success">ADICIONAR</button>
                     <?php
                         if ($id == 'crud') {
    	                     echo("<a class='btn btn-info' href='crud.php'>VOLTAR</a>");
                         } else {
                             echo("<a class='btn btn-info' href='index.html'>VOLTAR</a>");
                         }
	                ?>
                </div>
            </form>
          </div>
        </div>
        </div>
    </div>
    </div>
</body>
</html>