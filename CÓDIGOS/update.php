<?php
	require 'conecta.php';

	$cod_usuario = null;
	if (!empty($_GET['cod_usuario'])) {
		$cod_usuario = $_REQUEST['cod_usuario'];
    }

	if (null==$cod_usuario ) {
		header("Location: crud.php");
    }

	if (!empty($_POST)) {
		$username = $_POST['username'];
		$senha = $_POST['senha'];
		$perfil = $_POST['perfil'];
        $email = $_POST['email'];
        $nome = $_POST['nome'];
        $cpf_cnpj = $_POST['cpf_cnpj'];
		
		$validacao = true;
		if (empty($nome)) {
            $nomeErro = 'Por favor digite o nome!';
            $validacao = false;
        }

        //*** Mais Validação aqui depois!!! ***

		if ($validacao) {
            $pdo = bdNema::conectar();
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $sql = "UPDATE Usuarios set username = ?, senha = ?, cod_perfil = ?, email = ?, nome = ?, cpf_cnpj = ?  WHERE cod_usuario = ?";
            
            $q = $pdo->prepare($sql);
            $q->execute(array($username,$senha,$perfil,$email,$nome,$cpf_cnpj,$cod_usuario));
            
            bdNema::desconectar();
            header("Location: crud.php");
		}
	} else {
        $pdo = bdNema::conectar();
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		$sql = "SELECT * FROM Usuarios where cod_usuario = ?";
		$q = $pdo->prepare($sql);
		$q->execute(array($cod_usuario));
		$data = $q->fetch(PDO::FETCH_ASSOC);
		
		$username = $data['username'];
		$senha = $data['senha'];
		$perfil = $data['cod_perfil'];
        $email = $data['email'];
        $nome = $data['nome'];
        $cpf_cnpj = $data['cpf_cnpj'];
		
		bdNema::desconectar();
}
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
	<title>Atualizar Contato</title>
</head>
<body>
    <div class="container">
        <h3 class="well"> Atualizar Usuário</h3>
        <form class="form-horizontal" action="update.php?cod_usuario=<?php echo $cod_usuario?>" method="post">
           <div class="col-xs-6">
                <label for="username">Username:</label>
                <input name="username" type="text" value="<?php echo ($username);?>">
                <br>
            
                <label for="senha">Senha:</label>
                <input name="senha" type="text" value="<?php echo ($senha);?>">
                <br>
            
                <label for="senha">Cód. perfil:</label>
                <input name="perfil" type="text" value="<?php echo ($perfil);?>">
                <br>
                
                <label for="email">Email:</label>
                <input name="email" type="text" value="<?php echo ($email);?>">
                <br>
           
                <label for="nome">Nome:</label>
                <input name="nome" type="text" value="<?php echo ($nome);?>">
                <br>
           
                <label for="cpf_cnpj">CPF / CNPJ:</label>
                <input name="cpf_cnpj" type="text" value="<?php echo ($cpf_cnpj);?>">
                <br>
            
                <button type="submit" class="btn btn-info">ATUALIZAR</button>
                <a class="btn btn-info" href="crud.php">VOLTAR</a>
             </div>
        </form>
    </div>
</body>
</html>