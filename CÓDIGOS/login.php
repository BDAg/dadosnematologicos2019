<?php
    require 'conecta.php';    

    $cod_usuario = 0;
	$perfil      = 1;

    if(!empty($_POST)) {
        $perfil     = $_POST['perfil'];
        $usuario    = $_POST['usuario'];
        $senha      = $_POST['senha'];
        $usuario    = strtolower($usuario);

        $pdo = bdNema::conectar();

        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM Usuarios WHERE usuario = '$usuario'";
        $q = $pdo->prepare($sql);
        $q->execute();

        if ($q->rowCount() == 0) {      // Não retornou nada: usuário não existe
             $err = 1;
            goto sair_com_erro;   
        }

        $data = $q->fetch(PDO::FETCH_ASSOC);
        $cod_usuario = $data['cod_usuario'];
        $cod_perfil  = $data['cod_perfil'];
        $senha       = $data['senha'];

        if ($data['senha'] != $senha) { // Senha incorreta
            $err = 2;
            goto sair_com_erro;        
        }

        if ($data['cod_perfil'] != $perfil) { // Perfil incorreto
             $err = 3;
            goto sair_com_erro;     
        }
        
        // Registra data do último acesso
        $datahora = date('d-m-Y H:i:s');   // *** Dando erro com timezone - definir zona ***

        $sql = "UPDATE Usuarios SET ultimo_acesso = ? WHERE cod_usuario = ?";
        $q2 = $pdo->prepare($sql);
        $q2->execute(array($datahora,$cod_usuario));
        
        if ($perfil == 1) { // Se Cliente
            $sql = "SELECT * FROM Clientes WHERE cod_usuario = $cod_usuario";
        } else {
            $sql = "SELECT * FROM Prestadores WHERE cod_usuario = $cod_usuario";
        }
        
        $q = $pdo->prepare($sql);
        $q->execute();
        $data = $q->fetch(PDO::FETCH_ASSOC);  
 
        setcookie("cod_usuario",$cod_usuario);

        if ($perfil == 1) { // Se Cliente, carrega inicio.php, senão carrega homelab.php
            $nome_cliente = $data['nome_cliente'];
            setcookie("nome_cliente",$nome_cliente);
            setcookie("cod_cliente",$cod_cliente);

        	header("Location: inicio.php"); 
        } else {
            $nome_prestador = $data['nome_prestador'];
            $cod_prestador  = $data['cod_prestador'];
            setcookie("nome_prestador",$nome_prestador);
            setcookie("cod_prestador",$cod_prestador);
            
			header("Location: homelab.php");      
        }

        bdNema::desconectar();

        exit(0);
sair_com_erro:
        bdNema::desconectar();
        header("Location: login.php?err=$err&perfil=$perfil");
    }
?>

<!doctype html>

<html lang="pt">

  <head>

    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="">

    <title>Login</title>

    <link href="bootstrap/bootstrap.min.css" rel="stylesheet">

  </head>

  <body>

    <div class="container">

        <div class="row justify-content-center align-items-center" style="height:100vh">

            <div class="col-4" align="center">
                

                <div class="card">
                    <?php
                        $perfil = $_GET[perfil];
                        $err      = $_GET[err];

                        if ($perfil == 1) {
                             echo ("<p class='lead'><b>Login do Cliente</b></p>");
                            } else {
                              echo ("<p class='lead'><b>Login do Prestador de Serviço</b></p>");
                        }

                        switch ($err) {
                            case 1:
                                echo "<p><b><font color=\"#FF0000\">Usuário não existe!</font></b></p>";
                                break;
                            case 2:
                                echo "<p><b><font color=\"#FF0000\">Senha incorreta!</font></b></p>";
                                break;
                            case 3:
                                echo "<p><b><font color=\"#FF0000\">Perfil incorreto!</font></b></p>";
                                break;
                        }
                ?>
                    <div class="card-body">

                        <form method="post" action="login.php">
                            <?php  echo ("<input type='hidden' id='perfil' name='perfil' value='" . $perfil . "'>"); ?>
                            
                            <div class="form-group">

                                <input type="text" class="form-control" name="usuario" placeholder="nome de usuário" required>

                            </div>

                            <div class="form-group">

                                <input type="password" class="form-control" name="senha" placeholder="senha" required>

                            </div>

                            

                            <p><a class="text-center" href="recsenha.php">Esqueci minha senha</a></p>

                            

                           <button type="submit" class="btn btn-primary mb-3">ENTRAR</button>

             

                <button type="button" class="btn btn-primary mb-3" onclick="location.href = 'index.html'">VOLTAR</button>

                        </form>

                    </div>

                </div>

            </div>

        </div>

    </div>

    </body>

</html>