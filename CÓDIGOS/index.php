<?php
    // Apaga os cookies
    if (isset($_COOKIE['cod_usuario'])) {
        unset($_COOKIE['cod_usuario']);
    }
     if (isset($_COOKIE['cod_cliente'])) {
        unset($_COOKIE['cod_cliente']);
        
    }  
    if (isset($_COOKIE['cod_prestador'])) {
        unset($_COOKIE['cod_prestador']);
        
    }  
    if (isset($_COOKIE['nome_cliente'])) {
        unset($_COOKIE['nome_cliente']);
        
    } 
     if (isset($_COOKIE['nome_prestador'])) {
        unset($_COOKIE['nome_prestador']);
    }   
?>
<!DOCTYPE html>

<html lang="pt">
    <head>
        <meta charset="utf-8">
        <title>Análise de Dados Nematológicos</title>
         <link rel="stylesheet" href="bootstrap/bootstrap.css">
         <style>
            body {
            background: rgb(95, 153, 240);
        }
    </style>
    </head>
    <body>
        <div align="center">
            <div>
                <h1 class="display-4">Análise de Dados Nematológicos</h1>
                <p class="lead">Faça o login para acessar o sistema. Caso ainda não possua uma conta, cadastre-se!</p>
                <div>
                    <img class="img" src="/img/logo1.jpg" alt="">
                </div>
                <div>
                    <p></p>
                    <button type="button" class="btn btn-primary" onclick="location.href = 'create.php'">SOU NOVO, QUERO ME CADASTRAR</button>
                    <button type="button" class="btn btn-primary" onclick="location.href = 'login.php?perfil=1'">LOGIN CLIENTE</button>
                    <button type="button" class="btn btn-primary" onclick="location.href = 'login.php?perfil=2'">LOGIN PRESTADOR DE SERVIÇOS</button>
                </div>
            </div>
        </div>
    </body>
</html>