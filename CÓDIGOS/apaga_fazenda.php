<?php
    require 'conecta.php';
    
    $cod_cliente = $_GET['cod_cliente'];
    $cod_fazenda = $_GET['cod_fazenda'];
    $pdo = bdNema::conectar();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Apaga registro na tab. Fazendas
    $sql = "DELETE FROM Fazendas WHERE cod_cliente = $cod_cliente";
    $q = $pdo->prepare($sql);
    $q->execute();
    
    // Apaga os talhões na tab. Talhoes
    $sql = "DELETE FROM Talhoes WHERE cod_fazenda = $cod_fazenda";
    $q = $pdo->prepare($sql);
    $q->execute();
    
    bdNema::desconectar();
    //header("Location: crud.php");
?>