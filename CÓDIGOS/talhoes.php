<?php
    require 'conecta.php';
    
    // Cód. fazenda passado por fazendas.php
     if (isset($_GET['cod_cliente'])) {
        $cod_fazenda = $_GET['cod_fazenda'];
    }
      
   
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>Talhões</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <link href="/bootstrap/bootstrap.min.css" rel="stylesheet">
      <script>
      var $cod = 0;
        $(document).ready(function(){
            $("input:checkbox").on('click', function() {
                var $box = $(this);
                if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
              } else {
                $box.prop("checked", false);
              }
              
              if(this.checked) {
                $cod = this.value;
                } else {
                   $cod = 0; 
                }
            });
            
            $('#btnexcluir').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Sim": function() {
                            $(this).dialog("close");
                            
                            var link = "delete.php?cod_usuario=" + $cod;  
                            window.open(link, "_parent");
                        },
                        Não: function() {
                          $(this).dialog("close");
                        }
                      }
                    });
                }
            });
            
        $('#btninfo').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                    var link = "read.php?cod_usuario=" + $cod;
                    window.open(link, "_blank");
                }
            });    
            
            $('#btnatualizar').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                    var link = "update.php?cod_usuario=" + $cod;
                    window.open(link, "_blank", "width=200,height=100");
                }
            }); 
            
        });
      </script>
    </head>    
    <body>
    <div class="card">
        <div id="dialog-confirm" title="Excluir registro?" style="display:none">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Este registro será excluído permanentemente. Tem certeza?</p>
        </div>
    
        <div id="dialog-alert1" title="CRUD" style="display:none">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Selecione um registro!</p>
        </div>
        <div class="card-header">
            <h3 class="well">Talhões</h3>
                <a class="btn btn-info" id="btnalterar"   href="create.php?id=crud">ALTERAR</a>
                <a class="btn btn-info" id="btnadicionar" href="create.php?id=crud">ADICIONAR</a>
                <a class="btn btn-info" id="btnexcluir"   href="#">EXCLUIR</a>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <th scope=""></th>
                        <th scope=""></th>
                    </tr>
                </thead>
                <tbody>
                    <?php
                        include 'conecta.php';
                        $pdo = bdNema::conectar();
                        $sql = 'SELECT * FROM Talhoes WHERE cod_fazenda=$cod_fazenda';
                           
                        foreach($pdo->query($sql)as $row) {
                            echo '<tr>';
                            echo '<td>' . '<input type="checkbox" name="chkcod[]" id="chkcod[]" value="' .  $row['cod_talhao'] . '">' . '</td>';
			                echo '<td>'. $row['nome_talhao'] . '</td>';
                            echo '</tr>';
                        }
                       
                        bdNema::desconectar();
                    ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
</body>
</html>