<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>Consulta de Laudo</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <link rel="stylesheet" href="/resources/demos/style.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="/bootstrap/bootstrap.min.css" rel="stylesheet">
      <script>
      var $cod = 0;
        $(document).ready(function(){
            $("input:checkbox").on('click', function() {
                var $box = $(this);
                if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
              } else {
                $box.prop("checked", false);
              }
              
              if(this.checked) {
                $cod = this.value;
                } else {
                   $cod = 0; 
                }
            });
            
            $('#btnexcluir').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height: "auto",
                    width: 400,
                    modal: true,
                    buttons: {
                        "Sim": function() {
                            $(this).dialog("close");
                            
                            var link = "delete.php?cod_laudo=" + $cod;  
                            window.open(link, "_parent");
                        },
                        Não: function() {
                          $(this).dialog("close");
                        }
                      }
                    });
                }
            });
            
        $('#btndetalhes').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                    var link = "read.php?cod_usuario=" + $cod;
                    window.open(link, "_blank");
                }
            });    
            
            $('#btnatualizar').click(function(){
                if ($cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                    var link = "update.php?cod_usuario=" + $cod; 
                    window.open(link, "_parent");
                }
            }); 
            
        });
      </script>
    </head>    
    <body>
        <div class="container-fluid">
            <h2>Consulta de Laudo - Cliente: <?php echo ($_COOKIE['nome_cliente']); ?></h2>
            <p>
            <div id="dialog-confirm" title="Excluir registro?" style="display:none">
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Este registro será excluído permanentemente. Tem certeza?</p>
            </div>
        
            <div id="dialog-alert1" title="LAUDO" style="display:none">
                <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Selecione um registro!</p>
            </div>
        
            <a class="btn btn-info" href="homelab.php">VOLTAR</a>
            <a class="btn btn-info" href="#" id="btndetalhes">DETALHES</a>
            <a class="btn btn-info" href="#" id="btnexcluir">EXCLUIR</a>
            </p>
            <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope="col"></th>
                            <th scope="col">Cod.</th>
                            <th scope="col">Data</th>
                            <th scope="col">Fazenda</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            include 'conecta.php';
                            $pdo = bdNema::conectar();
                            $sql = "SELECT Laudos.cod_laudo, Laudos.data_laudo, Fazendas.nome_fazenda FROM Laudos INNER JOIN Fazendas ON Laudos.cod_fazenda = Fazendas.cod_fazenda";
                               
                            foreach($pdo->query($sql)as $row) {
                                echo '<tr>';
                                echo '<td>' . '<input type="checkbox" name="chkcod[]" id="chkcod[]" value="' .  $row['cod_laudo'] . '">' . '</td>';
                                
    			                echo '<th scope="row">'. $row['cod_laudo'] . '</th>';
    			                echo '<td>'. $row['data_laudo'] . '</td>';
    			                echo '<td>'. $row['nome_fazenda'] . '</td>';
                                echo '</tr>';
                            }
                           
                            bdNema::desconectar();
                        ?>
                    </tbody>
                </table>
        </div>
    </body>
</html>