<?php
	require 'conecta.php';

	$cod_usuario = null;
	if (!empty($_GET['cod_usuario'])) {
		$cod_usuario = $_REQUEST['cod_usuario'];
    }

	if (null==$cod_usuario ) {
		header("Location: crud.php");
    }

	$pdo = bdNema::conectar();
	$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
    $sql = "SELECT * FROM Usuarios WHERE cod_usuario = $cod_usuario";
	$q = $pdo->prepare($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	$usuario = $data['usuario'];
	$senha    = $data['senha'];
	$perfil   = $data['cod_perfil'];
	$ult_acesso = $data['ultimo_acesso'];
    
    if ($perfil == 1) {     // Cliente
        $sql = "SELECT * FROM Clientes WHERE cod_usuario = $cod_usuario";
        $q = $pdo->prepare($sql);
        $q->execute();
    	$data = $q->fetch(PDO::FETCH_ASSOC);
    	$email       = $data['email_cliente'];
        $nome        = $data['nome_cliente'];
        $cpf         = $data['cpf_cliente'];
        $cod_cliente = $data['cod_cliente'];
    } else {                // Prestador
        $sql = "SELECT * FROM Prestadores WHERE cod_usuario = $cod_usuario";
        $q = $pdo->prepare($sql);
	    $q->execute();
	    $data = $q->fetch(PDO::FETCH_ASSOC);
	    $email         = $data['email_prestador'];
        $nome          = $data['nome_prestador'];
        $cnpj          = $data['cnpj_prestador'];
        $cod_prestador = $data['cod_prestador'];
    }
	
	bdNema::desconectar();
?>

<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
	<title>Informações do Usuário</title>
	</head>
<body>
    <div class="container">
        <div class="jumbotron">
            <p><h3 class="well">Informações do Usuário</h3></p>
            <hr size="6" width="50%" align="left" color="green">
            
            <?php
                if ($perfil == 1) {
                    echo ('<label>Cód. cliente: </label>');
                    echo ('<span class="label label-info"><b>' . $cod_cliente . '</b></span>');
                } else {
                    echo ('<label>Cód. prestador serviço: </label>');
                    echo ('<span class="label label-info"><b>' . $cod_prestador . '</b></span>');
                }
            ?>
            
            <br>
            <label>Cód. usuário: </label>
            <span class="label label-info"><b><?php echo ($cod_usuario);?></b></span>
            <br>
            
            <label>Usuário: </label>
            <span class="label label-info"><b><?php echo ($usuario);?></b></span>
            <br>
            
            <label>Senha: </label>
            <span class="label label-default"><b><?php echo ($senha);?></b></span>
            <br>
            
            <label>Perfil: </label>
            <?php
            if ($perfil == 1) {
                echo ('<span class="label label-default"><b>1 - Cliente</b></span>');
            } else {
                echo ('<span class="label label-default"><b>2 - Prestador de serviço</b></span>');
            }
            ?>
            <br>
            
            <label >Email: </label>
            <span class="label label-default"><b><?php echo ($email);?></b></span>
            <br>
            
            <label>Nome: </label>
            <span class="label label-default"><b><?php echo ($nome);?></b></span>
            <br>
            
            <?php
                if ($perfil == 1) {
                    echo ('<label>CPF: </label>');
                    echo ('<span class="label label-default"><b>' . $cpf . '</b></span>');
                } else {
                    echo ('<label>CNPJ: </label>');
                    echo ('<span class="label label-default"><b>' . $cnpj. '</b></span>');
                }
            ?>
            
            <br>
            
            <label>Últ. acesso: </label>
            <span class="label label-default"></span>
            <span class="label label-default"><b><?php echo ($ult_acesso);?></b></span>
            <br>
        </div>
    </div>
</body>
</html>