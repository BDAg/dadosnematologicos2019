
<?php
    require 'conecta.php';
    
    // Cód. cliente passado por cadastro_laudo_cliente.php (seleção do cliente)
    if (isset($_GET['cod_cliente'])) {
        $cod_cliente = $_GET['cod_cliente'];
        
        $pdo = bdNema::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM Clientes WHERE cod_cliente = $cod_cliente";
        $q = $pdo->prepare($sql);
        $q->execute();
       
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $nome_cliente = $data['nome_cliente'];
        $cpf_cliente  = $data['cpf_cliente'];
        
        bdNema::desconectar();
    }
    
    if (!empty($_POST)) {  // Botão Adicionar foi clicado! (Submit) 
        $dtlaudo     = $_POST['dtlaudo'];
    	$fazenda     = $_POST['fazenda'];
		$nematoide   = $_POST['nematoide'];
		$talhao      = $_POST['talhao'];
        $solo        = $_POST['solo'];
        $raiz        = $_POST['raiz'];
        $ovos        = $_POST['ovos'];
        
        $pdo = bdNema::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $sql = "SELECT * FROM Clientes WHERE cod_cliente = $cod_cliente";
        $q = $pdo->prepare($sql);
        $q->execute();
       
        $data = $q->fetch(PDO::FETCH_ASSOC);
        $nome_cliente = $data['nome_cliente'];
        $cpf_cliente  = $data['cpf_cliente'];
            
        $sql = "INSERT INTO Laudos (cod_cliente,data_laudo, cod_fazenda, cod_talhao, cod_nematoide, solo, raiz, ovos) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $q = $pdo->prepare($sql); 
        $q->execute(array($cod_cliente,$dtlaudo,$fazenda,$talhao,$nematoide,$solo,$raiz,$ovos));   
        bdNema::desconectar();
    }           // (!empty($_POST))
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <title>Inclusão de Laudo</title>
    
    <script>
        $(document).ready(function(){
            $("#fazenda").change(function(){
                var cod = $(this).val();

                $.ajax({
                    url     : 'ret_talhoes.php',
                    type    : 'post',
                    data    : {cod_fazenda:cod},
                    dataType: 'json',
                    success :function(response){

                        var len = response.length;

                        $("#talhao").empty();
                        for (var i = 0; i<len; i++){
                            var cod  = response[i]['cod'];
                            var nome = response[i]['nome'];

                            $("#talhao").append("<option value='"+cod+"'>"+nome+"</option>");
                        }
                    }
                });
            });
            
        });
    </script>
</head>
<body>
    <div class="container">
        <div clas="span10 offset1">
          <div class="card">
            <div class="card-header">
                <h3 class="well" align="center">Inclusão de Laudo</h3>
                <?php
                    echo ('<h4 class="well" align="center">Cliente: ' . $nome_cliente . ' / CPF: ' . $cpf_cliente . '</h4>');
                ?>
            </div>
           
            <div class="card-body">
                <form class="form-horizontal" action='cadastro_laudo.php' method="post">
                
                <div class="mb-3">
                    <label><b>Data do Laudo:</b></label>
                   <input id="dtlaudo" name="dtlaudo" type="date" required>
                </div>
                
                <div class="mb-3">
                    <label><b>Fazenda:</b></label>
                    <select id="fazenda" name="fazenda" required>
                    <option value="" disabled selected>== Selecione uma fazenda ==</option>
                    <?php
                        $pdo = bdNema::conectar();
                        $sql = "SELECT * FROM Fazendas WHERE cod_cliente = $cod_cliente";
                        foreach($pdo->query($sql)as $row) {
                            echo ("<option value='" . $row['cod_fazenda'] . "'>" . $row['nome_fazenda'] . "</option>");
                        }
                        bdNema::desconectar();
                    ?>
                    </select>
                </div>
           
                <div class="mb-3">
                    <label><b>Talhão:</b></label>
                    <select id="talhao" name="talhao" required>
                        <option value="">== Selecione um talhão ==</option>
                    </select>
                </div>
                
                <div class="mb-3">
                    <label><b>Nematoide:</b></label>
                    <select name="nematoide" required>
                    <option value="" disabled selected>== Selecione um nematoide ==</option>
                    <?php
                        $pdo = bdNema::conectar();
                        $sql = 'SELECT * FROM Nematoides';
                         
                        foreach($pdo->query($sql)as $row) {
                            echo ("<option value='" . $row['codigo_nematoide'] . "'>" . $row['especie_nematoide'] . " " . $row['genero_nematoide'] . "</option>");
                        }
                        echo ("</select>");
                        bdNema::desconectar();
                    ?>
                </div>
                
                <div class="mb-3">
                    <label><b>Solo:</b></label>
                    <input type="number" name="solo" id="solo" min="0" max="1000" required>
                </div>
                
                <div class="mb-3">
                    <label><b>Raiz:</b></label>
                    <input type="number" name="raiz" id="raiz" min="0" max="1000" required>
                </div>
                
                <div class="mb-3">
                    <label><b>Ovos:</b></label>
                    <input type="number" name="ovos" id="ovos" min="0" max="1000" required>
                </div>
        		
                <div class="form-actions">
                    <br/>
                    <input class="btn btn-info" type="submit" value="ADICIONAR">
    	            <a class="btn btn-info" href="homelab.php">VOLTAR</a>
    	            <?php
                        echo ("<a class='btn btn-info' href='fazendas.php?cod_cliente=" . $cod_cliente . "'>FAZENDAS & TALHÕES...</a>"); 
                    ?>
                </div>
            </form>
          </div>
        </div>
        </div>
    </div>
    </div>
</body>
</html>