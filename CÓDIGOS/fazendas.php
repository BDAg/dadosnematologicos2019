<?php
    require 'conecta.php';
    
    // Cód. cliente passado por cadastro_laudo.php
     if (isset($_GET['cod_cliente'])) {
        $cod_cliente = $_GET['cod_cliente'];
    }
   
?>
<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8"> 
        <meta charset="utf-8">
        <title>Fazendas</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <link href="/bootstrap/bootstrap.min.css" rel="stylesheet">
      <script>
        var cod = 0;
        $(document).ready(function(){
            $("input:checkbox").on('click', function() {
                var $box = $(this);
                if ($box.is(":checked")) {
                var group = "input:checkbox[name='" + $box.attr("name") + "']";
                $(group).prop("checked", false);
                $box.prop("checked", true);
              } else {
                $box.prop("checked", false);
              }
              
              if(this.checked) {
                cod = this.value;
                } else {
                   cod = 0; 
                }
            });
            
            $('#btnexcluir').click(function(){
                if (cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height   : "auto",
                        width    : 400,
                        modal    : true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                
                $("#dialog-confirm").dialog({
                    resizable: false,
                    height   : "auto",
                    width    : 400,
                    modal    : true,
                    buttons: {
                        "Sim": function() {
                            $(this).dialog("close");
                            
                            var cli = $('#cod_cli').val();
                            var link = "apaga_fazenda.php?cod_cliente=" + cli + "&cod_fazenda=" + cod;  
                            window.open(link, "_parent");
                        },
                        Não: function() {
                          $(this).dialog("close");
                        }
                      }
                    });
                }
            });
            
        $('#btntalhoes').click(function(){
            if (cod == 0) {
               $("#dialog-alert1").dialog({
                    resizable: false,
                    height   : "auto",
                    width    : 400,
                    modal    : true,
                    buttons: {
                        OK: function() {
                          $(this).dialog("close");
                        }
                      }
                });
            } else { 
                var link = "talhoes.php?cod_fazenda=" + cod;
                window.open(link, "_blank", "width=500,height=600");
            }
            });    
            
            $('#btnatualizar').click(function(){
                if (cod == 0) {
                   $("#dialog-alert1").dialog({
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            OK: function() {
                              $(this).dialog("close");
                            }
                          }
                    });
                } else { 
                    var link = "" + cod; 
                    window.open(link, "_parent");
                }
            }); 
            
        });
      </script>
    </head>    
    <body>
    <div class="container-fluid">
        <div id="dialog-confirm" title="Excluir registro?" style="display:none">
        <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Este registro será excluído permanentemente (os talhões relacionados também). Tem certeza?</p>
        </div>
    
        <div id="dialog-alert1" title="CRUD" style="display:none">
            <p><span class="ui-icon ui-icon-alert" style="float:left; margin:12px 12px 20px 0;"></span>Selecione um registro!</p>
        </div>
            
        <div class="card">
            <div class="card-header">
                <h3 class="well">Fazendas</h3>
                    <?php  echo ("<input type='hidden' id='cod_cli' name='cod_cli' value='" . $cod_cliente . "'>"); ?>
                 
                    <a class="btn btn-info" id="btnalterar"   href="#">ALTERAR</a>
                    <a class="btn btn-info" id="btnadicionar" href="#">ADICIONAR</a>
                    <a class="btn btn-info" id="btnexcluir"   href="#">EXCLUIR</a>
                    <a class="btn btn-info" id="btntalhoes"   href="#">TALHÕES...</a>
                    
    
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th scope=""></th>
                            <th scope=""></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $pdo = bdNema::conectar();
                            $sql = "SELECT * FROM Fazendas WHERE cod_cliente=$cod_cliente";
                               
                            foreach($pdo->query($sql)as $row) {
                                echo '<tr>';
                                echo '<td>' . '<input type="checkbox" name="chkcod[]" id="chkcod[]" value="' .  $row['cod_fazenda'] . '">' . '</td>';
    			                echo '<td>'. $row['nome_fazenda'] . '</td>';
                                echo '</tr>';
                            }
                           
                            bdNema::desconectar();
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
 </div>
</body>
</html>