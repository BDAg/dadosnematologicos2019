
<?php
    require 'conecta.php';

    if (!empty($_POST)) {  // Botão Adicionar foi clicado! (Submit)  
		$nome_fazenda   = $_POST['nome_fazenda'];
	//	$talhao1        = $_POST['talhao1'];
	//	$talhao2        = $_POST['talhao2'];
//		$talhao3        = $_POST['talhao3'];
		//$talhao4        = $_POST['talhao4'];
		
		// Cód. cliente passado por cadastro_laudo.php
    // Mudar esse esquema depois...
    $cod_cliente = $_COOKIE['cod_cli'];


        $pdo = bdNema::conectar();
        $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
      
        // Insere registro na tabela Fazendas
        $sql = "INSERT INTO Fazendas (nome_fazenda, cod_cliente VALUES (?, ?)";
        $q = $pdo->prepare($sql);
        $q->execute(array($nome_fazenda,$cod_cliente));     
            
        
        
        bdNema::desconectar();
        //header("Location: cadastro_laudo.php");
       
    }           // (!empty($_POST))   
?>
<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="utf-8">
    <link rel="stylesheet" href="/bootstrap/bootstrap.min.css">
    <title>Adicionar Usuário</title>
</head>
<body>
    <div class="container">
        <div clas="span10 offset1">
          <div class="card">
            <div class="card-header">
                <h3 class="well" align="center"><b></b>Cadastro de Fazenda e Talhões</b></h3>
                <?php 
                    echo ("Cód. cliente: " . $cod_cliente); 
                      
                ?>
            </div>
            <div class="card-body">
	            <form class="form-horizontal" action="cadastro_fazenda.php" method="post"
                    <div class="mb-3">
                        <input type="text" class="form-control" name="nome_fazenda" placeholder="Nome da fazenda" required>
                    </div>

                    <div class="mb-3">
                        <input type="text" class="form-control" name="talhao1" placeholder="Identificação do talhão" required>
                    </div>

                    <div class="mb-3">
                        <input type="text" class="form-control" name="talhao2" placeholder="Identificação do talhão">
                    </div>
                    
                    <div class="mb-3">
                        <input type="text" class="form-control" name="talhao3" placeholder="Identificação do talhão">
                    </div>
                    
                    <div class="mb-3">
                        <input type="text" class="form-control" name="talhao4" placeholder="Identificação do talhão">
                    </div>
            	
                    <div class="form-actions">
                        <br/>
                        <button type="submit" class="btn btn-success">ADICIONAR</button>
                        <a class='btn btn-info' href='cadastro_laudo.php'>VOLTAR</a>
                    </div>
                </form>
          </div>
        </div>
        </div>
    </div>
    </div>
</body>
</html>