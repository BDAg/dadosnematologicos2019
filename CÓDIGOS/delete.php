<?php
    require 'conecta.php';
    
    $cod_usuario = $_GET['cod_usuario'];
    $pdo = bdNema::conectar();
    $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    
    // Pega o cód. perfil
    $sql = "SELECT * FROM Usuarios WHERE cod_usuario = $cod_usuario";
    $q = $pdo->prepare($sql);
	$q->execute();
	$data = $q->fetch(PDO::FETCH_ASSOC);
	$perfil = $data['cod_perfil'];
    
    // Apaga o usuário (tab. Usuários)
    $sql = "DELETE FROM Usuarios WHERE cod_usuario = $cod_usuario";
    $q = $pdo->prepare($sql);
    $q->execute();
    
    // Apaga o cliente/prestador (tab. Clientes/Prestadores) associado ao usuário
    if ($perfil == 1) {     // Cliente
        $sql = "DELETE FROM Clientes WHERE cod_usuario = $cod_usuario";
    } else {                // Prestador
        $sql = "DELETE FROM Prestadores WHERE cod_usuario = $cod_usuario";
    }
    $q = $pdo->prepare($sql);
    $q->execute();
    
    bdNema::desconectar();
    header("Location: crud.php");
?>