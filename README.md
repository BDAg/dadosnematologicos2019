# Dados Nematológicos - 2019

O objetivo deste projeto é automatizar a entrada, processamento e saída dos dados de análise nematológicos. 


Para acessar o **Cronograma** do projeto, clique [aqui](https://gitlab.com/BDAg/dadosnematologicos2019/wikis/home).


Para conhecer os **Membros da equipe**, clique [aqui](https://gitlab.com/BDAg/dadosnematologicos2019/wikis/Team).
